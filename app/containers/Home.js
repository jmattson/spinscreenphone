import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
	AppRegistry,
	DeviceEventEmitter,
	ListView,
	StyleSheet,
	Text,
	TouchableOpacity,
	TouchableHighlight,
	View,
  KeyboardAvoidingView,
  Modal,
	Image
} from 'react-native';
import t from 'tcomb-form-native';
import Chromecast from "react-native-google-cast";
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import uniqueId from 'react-native-unique-id';

// import API from './lib/api';
var defaultpass = 'VHczB0zJyC';

var Form = t.form.Form;

// here we are: define your domain model
var Item = t.struct({
  caption: t.String,              // a required string
  duration: t.Number               // a required number
});

var options = {}; // optional rendering options (see documentation)

class Home extends Component {

	constructor(props) {
		super(props);
		// this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.state = {
			dataSource: new ListView.DataSource({
	      rowHasChanged: (row1, row2) => row1 !== row2,
	    }),
			basic: true,
	    loaded: false,
      reload: false,
	    noitems: false,
	    numitems: 0,
			modalOpen: false,
      behavior: 'padding',
		}

    this.swapElement = this.swapElement.bind(this);
    this.updateOrderAPICall = this.updateOrderAPICall.bind(this);
		this.chromecastCastMedia = this.chromecastCastMedia.bind(this);
		this.getChromecasts = this.getChromecasts.bind(this);
		this.disconnectChromecast = this.disconnectChromecast.bind(this);
		this.startChromecast = this.startChromecast.bind(this);
		this.initChromecast = this.initChromecast.bind(this);
		this.onChange = this.onChange.bind(this);
		this.updateMetadata = this.updateMetadata.bind(this);

    window.changeScene = false;
    window.bucket = 'jeffdev';
	}

	componentDidUpdate () {
    // console.log("window.changeScene: " + window.changeScene);
    if (this.state.reload == true || window.changeScene == true) {
    	 this.fetchData();
       window.changeScene = false;
     }
	 }

	componentDidMount() {
		 // SplashScreen.hide();
		 console.log("Setting offset to -150");
		 this.setState({offset: -150});
		 Chromecast.startScan();
		 DeviceEventEmitter.addListener(Chromecast.DEVICE_AVAILABLE, (existance) => this.setState({chromecastAround: existance.device_available}));
		 DeviceEventEmitter.addListener(Chromecast.MEDIA_LOADED, () => {});
		 DeviceEventEmitter.addListener(Chromecast.DEVICE_CONNECTED, () => {this.chromecastCastMedia()});
		 this.init();
	 }

	fetchData() {

		let REQUEST_URL = 'https://' + window.bucket + '.s3.amazonaws.com/mymedia/localcontent/' + window.uniqueId + '/localcontent.json';
		// console.log("REQUEST_URL: " + REQUEST_URL);
		fetch(REQUEST_URL)
			.then((response) => {
					if (!response.ok) {
							window.numitems = 0;
							console.log('ERROR: ' + response.status);
							return response;
					}
					return response.json();
			})
			.then((responseData) => {
				// console.log("ITEMS3: " + JSON.stringify(responseData.items));

				if (responseData.items) {
          var items = null;
          if (this.state.dataSource._dataBlob) {
            // console.log("ITEMS4: " + JSON.stringify(this.state.dataSource._dataBlob.s1));
            items = this.state.dataSource._dataBlob.s1;
          }

          var responseDataItemsString = JSON.stringify(responseData.items);
          var itemsString = JSON.stringify(items);

					if (!items || responseDataItemsString != itemsString || this.state.reload == true) {
						window.numitems = responseData.items.length;
						this.setState({
							dataSource: this.state.dataSource.cloneWithRows(responseData.items),
							loaded: true,
              reload: false,
							noitems: false,
							numitems: responseData.items.length
						});
					}
				}
				else {
					if (this.state.loaded == false) {
						this.setState({
							loaded: true,
							noitems: true,
							reload: false,
							numitems: 0
						});
					}
				}
			})
			.done();
	}

	deleteRow(secId, rowId, rowMap) {
		console.log("Deleting Row");
		rowMap[`${secId}${rowId}`].closeRow();
		let newItems = this.state.dataSource._dataBlob.s1.slice();
    //console.log(JSON.stringify(newItems));
    // console.log("ROW ID: " + rowId);
		newItems.splice(rowId, 1);
    // console.log(JSON.stringify(newItems));
		this.setState({dataSource: this.state.dataSource.cloneWithRows(newItems)});
    var url = 'http://localhost:8082/mymedia/deleteLocalContentItemOnly';
    return fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'spinscreen-session': window.sid
      },
      body: JSON.stringify({
        index: rowId,
        playerId: null,
        isDrilldown: false
      })
    }).then((response) => response.json())
      .then((responseJson) => {
          console.log('Delete complete');
					window.numitems--;
          Chromecast.sendMessage("reload", (window.numitems--).toString());
          this.setState({reload: false});
          this.setState({dataSource: this.state.dataSource.cloneWithRows(newItems)});
          // this.fetchData();
      }).catch(function(error) {
        console.log('request failed', error)
    })
	}

  swapElement(array, indexA, indexB) {
    console.log("indexA: " + indexA);
    console.log("indexB: " + indexB);

    var tmp = array[indexA];
    array[indexA] = array[indexB];
    array[indexB] = tmp;
    console.log(array);
  }

  updateOrderAPICall(items) {
    var url = 'http://localhost:8082/mymedia/saveLocalContentOrder';
    return fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'spinscreen-session': window.sid
      },
      body: JSON.stringify({
        localContentItems: items
      })
    }).then((response) => response.json())
      .then((responseJson) => {
          console.log('Update order complete');
          console.log(JSON.stringify(responseJson));
          this.setState({reload: false});
          Chromecast.sendMessage("reload", window.numitems.toString());
          // return new Promise(resolve => setTimeout(resolve, responseJson));
          // return callback(null, responseJson);
          return responseJson;
      }).catch(function(error) {
        console.log('request failed', error)
        // return callback(error, null);
        return error;
        //return new Promise(resolve => setTimeout(resolve, responseJson));
    })
  }

  updateOrderUp(secId, rowId, rowMap) {
    let newItems = this.state.dataSource._dataBlob.s1.slice();
    this.swapElement(newItems, Number(rowId)-1, rowId);
    if (rowId > 0) {
      this.setState({dataSource: this.state.dataSource.cloneWithRows(newItems)});
      rowMap[`${secId}${rowId}`].closeRow();
      // updateOrderAPICall(items, function(err, result) {
      //   if (err) console.log("Error updating order: " + err);
      //   else console.log("Successfully updated order");
      // });
      this.updateOrderAPICall(newItems);
    }
  }

  updateOrderDown(secId, rowId, rowMap) {
    let newItems = this.state.dataSource._dataBlob.s1.slice();
    if (rowId < newItems.length - 1) {
      this.swapElement(newItems, rowId, Number(rowId)+1);
      rowMap[`${secId}${rowId}`].closeRow();
      // updateOrderAPICall(items, function(err, result) {
      //   if (err) console.log("Error updating order: " + err);
      //   else console.log("Successfully updated order");
      // });
      this.setState({dataSource: this.state.dataSource.cloneWithRows(newItems)});
      this.updateOrderAPICall(newItems);
    }
  }

	async initChromecast() {

		function sleep(ms) {
			return new Promise(resolve => setTimeout(resolve, ms));
		}

		await sleep(1000);
		Chromecast.sendMessage("init", window.uniqueId);
	}


	login(userId) {
	 return fetch('http://localhost:8082/login/', {
		 method: 'POST',
		 headers: {
			 'Accept': 'application/json',
			 'Content-Type': 'application/json',
		 },
		 body: JSON.stringify({
			 email: userId,
			 password: defaultpass
		 })
	 }).then((response) => response.json())
		 .then((responseJson) => {
			 // console.log(responseJson);
			 window.sid = responseJson.sid;
			 // window.instanceId = result.instanceId;
			 console.log('SID: ' + window.sid);
			 console.log("UNIQUE_ID: " + window.uniqueId);
			 this.initChromecast();
			 return responseJson;
		 }).catch(function(error) {
			 console.log('request failed', error)
	 })
	}

	signup(userId) {
	 return fetch('http://localhost:8082/signup/', {
		 method: 'POST',
		 headers: {
			 'Accept': 'application/json',
			 'Content-Type': 'application/json',
		 },
		 body: JSON.stringify({
			 email: userId,
			 password: defaultpass
		 })
	 }).then((response) => response.json())
		 .then((responseJson) => {
			 // console.log(responseJson);
			 return responseJson;
		 }).catch(function(error) {
			 console.log('request failed', error)
	 })
	}

	emailExists(userId) {
		return fetch('http://localhost:8082/emailExists?email=' + userId, {
			 method: 'GET',
			 headers: {
				 'Accept': 'application/json',
				 'Content-Type': 'application/json',
			 }
			}).then((response) => response.json())
			 .then((responseJson) => {
				 // console.log("EMAIL EXISTS: " + JSON.stringify(responseJson));
				 return responseJson;
			 }).catch(function(error) {
				 console.log('request failed', error)
		})
	}

	updateMetadata() {

	  this.setState({modalOpen: false});

		var value = this.refs.form.getValue();
		var index = this.state.index;
		var caption = value.caption;
		var duration = value.duration;

		console.log("INDEX: " + index);
		console.log("CAPTION: " + caption);
		console.log("DURATION: " + duration);

		 return fetch('http://localhost:8082/mymedia/updateMetadataLocalContentOnly', {
			 method: 'POST',
			 headers: {
				 'Accept': 'application/json',
				 'Content-Type': 'application/json',
				 'spinscreen-session': window.sid
			 },
			 body: JSON.stringify({
				 index: index,
				 caption: caption,
				 duration: duration
			 })
		 }).then((response) => response.json())
			 .then((responseJson) => {
				 console.log(responseJson);
				 this.setState({reload: true});
				 Chromecast.sendMessage("nav", index);
				 return responseJson;
			 }).catch(function(error) {
				 console.log('request failed', error)
		 })
	}

	async init() {
		 await this.startChromecast();

		 if (this.state.chromecastList[0]) {
			var chromecastId = this.state.chromecastList[0].id;
			console.log("CHROMECAST ID: " + this.state.chromecastList[0].id);
			uniqueId()
				// If there is a uniqueId already stored for the app, use that
				.then(id => {
					console.log("UNIQUE ID: " + id);
					window.uniqueId = chromecastId + ":" + id;
					// this.signup = this.signup.bind(this);
					this.emailExists(window.uniqueId)
					 .then(result => {
						 if (result.valid == true) {
							 console.log("Unique identifier exists");
							 this.login(window.uniqueId)
							 .then(result => {
								 this.fetchData();
							 })
						 }
						 else {
							 console.log("Unique identifier does not exist, creating new user " + chromecastId + ":" + id);
							 this.signup(window.uniqueId)
								 .then(result => {
									 if (result.valid == true) {
										 this.login(window.uniqueId)
										 .then(result => {
											 this.fetchData();
										 })
									 }
								 })
						 }
					 }, function(err) {
						 console.log(err);
					 })

				 //  this.login()
				 //    .then(function(result) {
				 //      window.sid = result.sid;
				 //      // window.instanceId = result.instanceId;
				 //      console.log('SID: ' + window.sid);
				 //    }, function(err) {
				 //      console.log(err);
				 //    })
				})
				// If no uniqueId, create a new user
				.catch(error => {
					 console.error(error);
				})
		 }
		 else {
			 console.log("Couldn't find chromecast");
		 }

	}

	disconnectChromecast(){
	 console.log('DISCONNECTING FROM CHROMECAST');
	 // Chromecast.disconnect();
	 this.setState({connected: false});
	}

	async startChromecast() {

		function sleep(ms) {
			return new Promise(resolve => setTimeout(resolve, ms));
		}

		await this.getChromecasts();
		if (this.state.chromecastList[0]) {
			await this.connectToChromecast(this.state.chromecastList[0].id);
			this.chromecastCastMedia();
		}
		// await sleep(20000);
		// Chromecast.sendMessage("nav", "3");
		// await sleep(15000);
		// Chromecast.sendMessage("nav", "2");
		// await sleep(15000);
		// Chromecast.sendMessage("nav", "1");
		// await sleep(15000);
		// Chromecast.sendMessage("nav", "0");
	}

	async getChromecasts() {

		function sleep(ms) {
		 return new Promise(resolve => setTimeout(resolve, ms));
		}

		// console.log('Taking a break...');
		await sleep(2000);
		// console.log('Two second later');
		let chromecastDevices = await Chromecast.getDevices();
		this.setState({chromecastList: chromecastDevices});
	}

	chromecastCastMedia() {
	 this.setState({connected: true});
	 Chromecast.castMedia('http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4', 'Video Test', 'http://camendesign.com/code/video_for_everybody/poster.jpg', 0);
	};

	async connectToChromecast(id) {
	 const isConnected = await Chromecast.isConnected();
	 isConnected
		 ? this.chromecastCastMedia()
		 : Chromecast.connectToDevice(id);
	}

	onChange(value) {
		// this.setState(value: value);
		console.log(JSON.stringify(value));
		console.log(this);
	}

	modalOpen(item, index) {

		this.setState({value: {caption: item.caption, duration:item.duration},
			index: index, caption: item.caption, duration: item.duration, modalOpen: true});
	}

	renderLoadingView() {
		return (
			<View style={styles.container2}>
				<Text>
					Loading...
				</Text>
			</View>
		);
	}

	renderNoItems() {
		return (
			<View style={styles.container2}>
				<Text style={styles.noitems}>
					 You have not added any images yet.  You will see your playlist on this page after you have added something.
				</Text>
			</View>
		);
	}

	render() {
		if (!this.state.loaded) {
			return this.renderLoadingView();
		}

		if (this.state.noitems) {
			return this.renderNoItems();
		}

		return (
			<View style={styles.container}>
					<SwipeListView
						dataSource={this.state.dataSource}
						renderRow={ (item, sectionID, rowID, highlightRow) => (
							<TouchableHighlight
								onPress={() => this.modalOpen(item, rowID)}
								style={styles.rowFront}
								underlayColor={'#AAA'}
							>
							<View>
								<Image
									source={{uri: item.thumbnail}}
									style={styles.thumbnail}
								/>
								<View>
									<Text style={styles.title}>{item.caption}</Text>
								</View>
							</View>
							</TouchableHighlight>
						)}
						renderHiddenRow={ (data, secId, rowId, rowMap) => (
							<View style={styles.rowBack}>
								<TouchableOpacity style={[styles.backLeftBtn, styles.backLeftBtnLeft]} onPress={ _ => this.deleteRow(secId, rowId, rowMap) }>
									<Text style={styles.backTextWhite}>Delete</Text>
								</TouchableOpacity>
								<TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnLeft]} onPress={ _ => this.updateOrderUp(secId, rowId, rowMap) }>
									<Text style={styles.backTextWhite}>Move Up</Text>
								</TouchableOpacity>
								<TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnRight]} onPress={ _ => this.updateOrderDown(secId, rowId, rowMap) }>
									<Text style={styles.backTextWhite}>Move Down</Text>
								</TouchableOpacity>
							</View>
						)}
						leftOpenValue={75}
						rightOpenValue={-150}
					/>
					<Modal
             animationType="slide"
             transparent={true}
						 visible={this.state.modalOpen}
             style={styles.modalStyle}>
						 <KeyboardAvoidingView behavior={this.state.behavior} style={styles.modalcontainer}>
                <View behavior={this.state.behavior} style={styles.modalcontainer2}>
                  {/* display */}
                  <Form
                    ref="form"
                    type={Item}
                    options={options}
										value={this.state.value}
										onChange={this.onChange}
                  />
                  <TouchableHighlight
                    style={styles.button}
                    onPress={() => this.updateMetadata()}
                    underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>Save</Text>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={styles.closeButton}
                    onPress={() => this.setState({modalOpen: false})}
                    underlayColor='#db524b'>
                    <Text style={styles.buttonText}>Close</Text>
                  </TouchableHighlight>
                </View>
						 </KeyboardAvoidingView>
					</Modal>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'white',
		flex: 0,
    marginTop: 20,
    marginBottom: 30,
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: 'white',
		borderBottomColor: 'black',
		borderBottomWidth: 1,
		justifyContent: 'center',
		height: 180,
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#DDD',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 60,
		width: 75,
		height: 75
	},
	backRightBtnLeft: {
		backgroundColor: '#56c0df',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#f2ae43',
		right: 0
	},
	backLeftBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 60,
		width: 75,
		height: 75
	},
	backLeftBtnLeft: {
		backgroundColor: '#db524b',
		// left: 75
	},
	controls: {
		alignItems: 'center',
		marginBottom: 30
	},
	switchContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginBottom: 5
	},
	switch: {
		alignItems: 'center',
		borderWidth: 1,
		borderColor: 'black',
		paddingVertical: 10,
		width: 100,
	},
	container2: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
		borderStyle: 'solid',
		borderBottomWidth: 1,
		borderBottomColor: 'grey',
		paddingTop: 10,
		paddingBottom: 10,
		// marginRight: 20,
		// marginLeft: 20,
	},
	noitems: {
		textAlign: 'center',
	},
	title: {
		fontSize: 12,
		textAlign: 'center',
		paddingTop: 2,
	},
	year: {
		textAlign: 'center',
	},
	thumbnail: {
		width: 250,
		height: 141,
		backgroundColor: 'grey',
	},
	listView: {
		paddingTop: 20,
		backgroundColor: '#F5FCFF',
	},
	modalcontainer: {
		// justifyContent: 'flex-start',
		// marginTop: 0,
    margin: 0,
    paddingTop: 40,
		padding: 20,
    height: 10000,
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
	},
  modalcontainer2: {
    // justifyContent: 'flex-start',
    // marginTop: 0,
    margin: 0,
    padding: 15,
    borderRadius: 8,
    backgroundColor: '#ffffff',
  },
	buttonText: {
		fontSize: 18,
		color: 'white',
		alignSelf: 'center'
	},
	button: {
		height: 36,
		backgroundColor: '#48BBEC',
		borderColor: '#48BBEC',
		borderWidth: 1,
		borderRadius: 8,
		marginBottom: 10,
		alignSelf: 'stretch',
		justifyContent: 'center'
	},
  closeButton: {
    height: 36,
    backgroundColor: '#db524b',
    borderColor: '#db524b',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});

function mapStateToProps(state) {
  return {
    searchedRecipes: state.searchedRecipes
  };
}
export default connect(mapStateToProps)(Home);
