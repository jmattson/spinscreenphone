import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  NativeModules,
  DeviceEventEmitter,
  ActivityIndicator,
  CameraRoll,
  TouchableOpacity,
  Modal,
} from 'react-native';
import ImageResizer from 'react-native-image-resizer';
import Chromecast from "react-native-google-cast";

var ImagePicker = NativeModules.ImageCropPicker;
var RNUploader = NativeModules.RNUploader;

var s3_policy = require('../lib/s3_policy');
let s3_opts = {
  bucket: 'jeffdev',
  region: 'us-west-2',
  key: 'AKIAI7WMKV7MEQ4CKOHA',
  secret: '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',
  type: 'image/',
  path: 'mymedia/uploads/jmattson@spinscreen.com/thumbs/',
  acl: 'public-read',
  expires: new Date(Date.now() + 60000),
  length: 10485760, // 10M as maximal size
};

class UploadFromCameraRoll extends Component {

  constructor(props) {
    super(props);

    this.state = {
      uploading: false,
      showUploadModal: false,
      uploadProgress: 0,
      uploadTotal: 0,
      uploadWritten: 0,
      uploadStatus: undefined,
      cancelled: false,
      images: [],
      thumbnails: [],
    }

    var uniqueId = window.uniqueId;
    s3_opts.path = 'mymedia/uploads/' + uniqueId + '/thumbs/';
  }

  componentDidMount() {
    DeviceEventEmitter.addListener('RNUploaderProgress', (data) => {
      let bytesWritten = data.totalBytesWritten;
      let bytesTotal   = data.totalBytesExpectedToWrite;
      let progress     = data.progress;
      this.setState({uploadProgress: progress, uploadTotal: bytesTotal, uploadWritten: bytesWritten});
    });
  }

  _addImage() {
    const fetchParams = {
      first: 25,
    };

    CameraRoll.getPhotos(fetchParams).then(
      (data) => {
        const assets = data.edges;
        const index = parseInt(Math.random() * (assets.length));
        const randomImage = assets[index];

        let images = [this.state.image];
        images.push(randomImage.node.image);

        console.log(images);
        this.setState({images: images});
    },
    (err) => {
      console.log(err);
    });
  }

  _closeUploadModal() {
    this.setState({showUploadModal: false, uploadProgress: 0, uploadTotal: 0, uploadWritten: 0, images: [], cancelled: false, });
  }

  _cancelUpload() {
    RNUploader.cancel();
    this.setState({uploading: false, cancelled: true});
  }

  // Generates random string
  randomString(length, chars) {
      var result = '';
      for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
      return result;
  }

  _uploadImages() {
    window.changeScene = true;

    let files = this.state.images.map( (file) => {
      return {
        name: 'file',
        filename: 'filename.jpg',
        filepath: file.uri,
        filetype: file.mime
      }
    });

    if (files.length > 0) {
      var image = {
        uri: files[0].filepath,
        type: files[0].filetype
      };

      var thumbnailUri = '';
      ImageResizer.createResizedImage(image.uri, 250, 141, 'JPEG', 100).then((resizedImageUri) => {
        console.log("RESIZED_IMAGE_URI: " + resizedImageUri);
        thumbnailUri = resizedImageUri;
        console.log("THUMBNAIL_URI: " + thumbnailUri);

        let thumbnails = [];
        thumbnails[0] = {
          name: 'file',
          filename: 'filename.jpg',
          filepath: thumbnailUri,
          filetype: image.type
        }
        var thumbnail = {
          uri: thumbnails[0].filepath,
          type: thumbnails[0].filetype
        };
        console.log(thumbnail.uri);


        // Update filename to be a random name for updates to the same file so it will be picked up properly
        var filenamesplit = files[0].filepath.split(".");
        var extension = filenamesplit[filenamesplit.length - 1];
        var rstring = this.randomString(24, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        var filename = rstring + "." + extension;
        var contenttype = files[0].filetype;
        console.log(contenttype);

        s3_opts.path = 'mymedia/uploads/' + uniqueId + '/';
        s3_opts.expires = new Date(Date.now() + 60000);
        let p = s3_policy(s3_opts);
        let opts = {
          url: 'https://' + s3_opts.bucket + '.s3.amazonaws.com/',
          files: files,
          params: {
            key: 'mymedia/uploads/' + window.uniqueId + '/' + filename,
            acl: s3_opts.acl,
            'X-Amz-Signature': p.signature,
            'x-amz-credential': p.credential,
            'X-Amz-Algorithm': 'AWS4-HMAC-SHA256',
            'X-Amz-Date': p.date + 'T000000Z',
            'Content-Type': contenttype,
            'policy': p.policy,
            'success_action_status': '201',
            'x-amz-meta-uuid': '14365123651274'
          }
        };

        this.setState({ uploading: true, showUploadModal: true, });
        RNUploader.upload(opts, (err, res) => {
          if (err) {
            console.log(err);
            return;
          }
          else {

            console.log('res');
            let status = res.status;
            let responseString = res.data;

            console.log("RESPONSE_STRING: " + responseString);

            s3_opts.path = 'mymedia/uploads/' + window.uniqueId + '/thumbs/';
            let opts2 = {
              url: 'https://' + s3_opts.bucket + '.s3.amazonaws.com/',
              files: thumbnails,
              params: {
                key: 'mymedia/uploads/' + window.uniqueId + '/thumbs/' + filename,
                acl: s3_opts.acl,
                'X-Amz-Signature': p.signature,
                'x-amz-credential': p.credential,
                'X-Amz-Algorithm': 'AWS4-HMAC-SHA256',
                'X-Amz-Date': p.date + 'T000000Z',
                'Content-Type': contenttype,
                'policy': p.policy,
                'success_action_status': '201',
                'x-amz-meta-uuid': '14365123651274'
              }
            };

            console.log('S3_OPTS_PATH: ' + s3_opts.path);
            console.log(opts2.params.key);
            RNUploader.upload(opts2, (err, res) => {
              if (err) {
                console.log(err);
                return;
              }
              else {
                var url = 'http://localhost:8082/mymedia/updateLocalContent';
                var filepath = 'https://' + s3_opts.bucket + '.s3.amazonaws.com/mymedia/uploads/' + window.uniqueId + '/' + filename;
                var thumbnailpath = 'https://' + s3_opts.bucket + '.s3.amazonaws.com/mymedia/uploads/' + window.uniqueId + '/thumbs/' + filename;
                console.log("FINISHED THUMBNAIL UPLOAD");
                return fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'spinscreen-session': window.sid
                  },
                  body: JSON.stringify({
                    url: filepath,
                    thumbnailUrl: thumbnailpath,
                    type: image.type
                  })
                }).then((response) => response.json())
                  .then((responseJson) => {
                      console.log('Upload complete with status ' + status);
                      this.setState({uploading: false, uploadStatus: status});
                      Chromecast.sendMessage("nav", window.numitems.toString());
                      window.numitems++;
                  }).catch(function(error) {
                    console.log('request failed', error)
                })
              }
            });
          }
        });
      }).catch((err) => {
        console.log(err);
      });
    }
  }

  uploadProgressModal() {
    let uploadProgress;

    if (this.state.cancelled) {
      uploadProgress = (
        <View style={{ margin: 5, alignItems: 'center', }}>
          <Text style={{ marginBottom: 10, }}>
            Upload Cancelled
          </Text>
          <TouchableOpacity style={ styles.button } onPress={ this._closeUploadModal.bind(this) }>
            <Text>Close</Text>
          </TouchableOpacity>
        </View>
      );
    } else if (!this.state.uploading && this.state.uploadStatus) {
      uploadProgress = (
        <View style={{ margin: 5, alignItems: 'center', }}>
          <Text style={{ marginBottom: 10, }}>
            Upload complete with status: { this.state.uploadStatus }
          </Text>
          <TouchableOpacity style={ styles.button } onPress={ this._closeUploadModal.bind(this) }>
            <Text>{ this.state.uploading ? '' : 'Close' }</Text>
          </TouchableOpacity>
        </View>
      );
    } else if (this.state.uploading) {
      uploadProgress = (
        <View style={{ alignItems: 'center', }}>
          <Text style={ styles.title }>Uploading { this.state.images.length } Image{ this.state.images.length == 1 ? '' : 's' }</Text>
          <ActivityIndicator
            animating={this.state.animating}
            style={[styles.centering, {height: 80}]}
            size="large" />
          <Text>{ this.state.uploadProgress.toFixed(0) }%</Text>
          <Text style={{ fontSize: 11, color: 'gray', marginTop: 5, }}>
            { ( this.state.uploadWritten / 1024 ).toFixed(0) }/{ ( this.state.uploadTotal / 1024 ).toFixed(0) } KB
          </Text>
          <TouchableOpacity style={ [styles.button, {marginTop: 5}] } onPress={ this._cancelUpload.bind(this) }>
            <Text>{ 'Cancel' }</Text>
          </TouchableOpacity>
        </View>
      );
    }

    return uploadProgress;
  }

  pickSingle(cropit) {
    ImagePicker.openPicker({
      width: 250,
      height: 141,
      cropping: cropit,
    }).then(image => {
      console.log('received image', image);
      var path = image.path;
      var image = {uri: path, width: image.width, height: image.height, mime: image.mime};
      var images = [image];
      this.setState({
        image: image,
        images: images,
      });
    }).catch(e => {
      console.log(e.code);
      alert(e);
    });
  }

  pickSingleWithCamera(cropping) {
    ImagePicker.openCamera({
      cropping,
      width: 500,
      height: 500
    }).then(image => {
      console.log('received image from camera', image);
      var path = image.path;
      var image = {uri: path, width: image.width, height: image.height, mime: image.mime};
      var images = [image];
      this.setState({
        image: image,
        images: images,
      });
    }).catch(e => alert(e));
  }

  pickMultiple() {
    ImagePicker.openPicker({
      multiple: true
    }).then(images => {
      console.log("images: " + images);
      this.setState({
        image: null,
        images: images.map(i => {
          console.log('received image', i);
          return {uri: i.path, width: i.width, height: i.height, mime: i.mime};
        })
      });
    }).catch(e => alert(e));
  }

  render() {
    return (
      <View style={styles.container}>

        <Modal
          animationType={'fade'}
          transparent={false}
          visible={this.state.showUploadModal}>

          <View style={styles.modal}>
            {this.uploadProgressModal()}
          </View>

        </Modal>

        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>

          <TouchableOpacity style={styles.button} onPress={() => this.pickSingleWithCamera(false)}>
            <Text>Camera</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={() => this.pickSingle(false)}>
            <Text style={styles.text}>Select From Gallery</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={this._uploadImages.bind(this)}>
            <Text>Upload</Text>
          </TouchableOpacity>

        </View>

        <View style={styles.thumbnailContainer}>
          {this.state.images.map((image) => {
            return <Image key={_generateUUID()} source={{uri: image.uri}} style={styles.thumbnail} />
          })}
        </View>

      </View>
    );
  }

}

function _generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};


var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    marginTop: 20,
    marginBottom: 30,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  thumbnail: {
    width: 300,
    height: 169.2,
    borderWidth: 0,
    borderColor: '#DDD',
    margin: 5,
  },
  thumbnailContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    // paddingTop: 10,
    paddingBottom: 10,
    marginRight: 20,
    marginLeft: 20,
  },
  modal: {
    margin: 50,
    borderWidth: 1,
    borderColor: '#DDD',
    padding: 20,
    borderRadius: 12,
    backgroundColor: 'lightyellow',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 14,
  },
  button: {
    borderWidth: 1,
    borderColor: '#CCC',
    borderRadius: 8,
    padding: 10,
    backgroundColor: '#EEE',
    marginHorizontal: 5,
  }
});

function mapStateToProps(state) {
  return {
    searchedRecipes: state.searchedRecipes
  };
}

export default connect(mapStateToProps)(UploadFromCameraRoll);
