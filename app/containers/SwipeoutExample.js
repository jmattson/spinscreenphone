//  include react-native-swipeout
import Swipeout from 'react-native-swipe-out';
//  example row data (see for json structure)
import rows from './data';
//  example styles
// import styles from './styles';

import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Image, ListView, Text, View} from 'react-native';

//  example swipout app
class SwipeoutExample extends Component {

  constructor() {
    super();

    //  datasource rerendered when change is made (used to set swipeout to active)
    var ds = new ListView.DataSource({rowHasChanged: (row1, row2) => true});

    this.state = {
      dataSource: ds.cloneWithRows(rows),
    };
  }

  _renderRow(rowData: string, sectionID: number, rowID: number) {
    return (
      <Swipeout
        left={rowData.left}
        right={rowData.right}
        rowID={rowID}
        sectionID={sectionID}
        autoClose={rowData.autoClose}
        backgroundColor={rowData.backgroundColor}
        close={!rowData.active}
        onOpen={(sectionID, rowID) => console.log(sectionID, rowID) }
        scroll={event => console.log('scroll event')}
      >
      <View style={styles.container}>
        <Image
          source={{uri: 'http://facebook.github.io/react/img/logo_og.png'}}
          style={styles.thumbnail}
        />
        <View>
          <Text style={styles.title}>This is a test</Text>
        </View>
      </View>
      </Swipeout>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.statusbar}/>
        <View style={styles.navbar}><Text style={styles.navbarTitle}>Swipeout</Text></View>
        <ListView
          scrollEnabled
          dataSource={this.state.dataSource}
          renderRow={this._renderRow.bind(this)}
          style={styles.listview}
        />
      </View>
    );
  }

}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    paddingTop: 10,
    paddingBottom: 10,
    marginRight: 20,
    marginLeft: 20,
  },
  noitems: {
    textAlign: 'center',
  },
  title: {
    fontSize: 12,
    textAlign: 'center',
    paddingTop: 8,
  },
  year: {
    textAlign: 'center',
  },
  thumbnail: {
    width: 250,
    height: 141,
    backgroundColor: 'grey',
  },
  listView: {
    paddingTop: 20,
    backgroundColor: '#F5FCFF',
  },
});

export default SwipeoutExample;
